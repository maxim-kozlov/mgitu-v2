# Mgitu V2

## Quick start
If you have auth token:
```bash
Mgitu.Console.App --host=https://gitlab.com/ --token=your-token --group=groupIntId -f input.csv
```

If you want to use username & password:
```bash
Mgitu.Console.App --host=https://gitlab.com/ -u user -p password --group="groupPath" -f input.csv
```

## Docker
```bash
docker run --volume=/path/to/input.csv:/input.csv maxkozlov/mgitu-cli \
    --host https://gitlab.com \
    -u user --token token \
    --group "test-group-123" \
    -f input.csv
```

where input.csv is text file in format:
Username1;@gitlab-login1
Username2;@gitlab-login2

use `--dry=true` to check repositories

Подробнее про формирование шаблона репозитория:
https://www.codeproject.com/Articles/1159258/SmartFormat-NET-Enhancing-string-Format-to-new-lev
