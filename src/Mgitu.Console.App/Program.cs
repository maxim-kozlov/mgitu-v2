﻿using CommandLine;
using CommandLine.Text;
using Mgitu.Console.App;
using Mgitu.Console.App.Extensions;
using Mgitu.Console.App.Gitlab;
using Mgitu.Console.App.Parsers;
using Mgitu.Console.App.Parsers.Csv;
using Mgitu.Console.App.Parsers.Excel;
using Mgitu.Console.App.Transliterate;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;


try
{
    var configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: true)
        .AddCliArgs<CliSettings>(args)
        .Build();

    if (args.Contains("-h") || args.Contains("--help"))
    {
        DisplayHelp();
        return;
    }

    Log.Logger = new LoggerConfiguration()
        .ReadFrom.Configuration(configuration)
        .CreateLogger();

    var servicesProvider = new ServiceCollection()
        .AddLogging(x => x.AddSerilog())
        .Configure<GitlabSettings>(configuration.GetRequiredSection("GitlabSettings"))
        .AddScoped<GitLabClientFactory>()
        .AddScoped<IUserInfoParser, UserInfoParser>()
        .AddScoped<UserInfoCsvParser>()
        .Configure<CsvParserSettings>(configuration.GetSection("CsvParserSettings"))
        .AddScoped<UserInfoExcelParser>()
        .Configure<ExcelParserSettings>(configuration.GetSection("ExcelParserSettings"))
        .AddSingleton<ITransliterator, Transliterator>()
        .Configure<TransliterateSettings>(configuration.GetSection("TransliteSettings"))
        .AddScoped<MainService>()
        .Configure<CliSettings>(configuration)
        .BuildServiceProvider();

    var mainService = servicesProvider.GetRequiredService<MainService>();
    await mainService.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "An Exception occured during bootstrapping!");
}
finally
{
    await Log.CloseAndFlushAsync();
}

static void DisplayHelp()
{
    var helpText = new HelpText
    {
        Heading = HeadingInfo.Default,
        Copyright = CopyrightInfo.Default,
        AddDashesToOption = true,
        MaximumDisplayWidth = 80
    };
    helpText = helpText.CustomBuild<CliSettings>();
    Console.WriteLine(helpText);
}