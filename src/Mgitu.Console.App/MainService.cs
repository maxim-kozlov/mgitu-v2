using System.ComponentModel.DataAnnotations;
using GitLabApiClient;
using GitLabApiClient.Models;
using GitLabApiClient.Models.Files.Requests;
using GitLabApiClient.Models.Groups.Responses;
using GitLabApiClient.Models.Projects.Requests;
using GitLabApiClient.Models.Users.Responses;
using Mgitu.Console.App.Gitlab;
using Mgitu.Console.App.Models;
using Mgitu.Console.App.Parsers;
using Mgitu.Console.App.Transliterate;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SmartFormat;

namespace Mgitu.Console.App;

public class MainService
{
    private readonly ILogger<MainService> _logger;
    private readonly GitLabClientFactory _gitLabClientFactory;
    private readonly IUserInfoParser _parser;
    private readonly IOptions<CliSettings> _settings;
    private readonly ITransliterator _transliterator;

    public MainService(ILogger<MainService> logger,
        GitLabClientFactory gitLabClientFactory,
        IUserInfoParser parser,
        IOptions<CliSettings> settings,
        ITransliterator transliterator)
    {
        _logger = logger;
        _gitLabClientFactory = gitLabClientFactory;
        _parser = parser;
        _settings = settings;
        _transliterator = transliterator;
    }

    public async Task Run()
    {
        var setting = _settings.Value;
        var client = await _gitLabClientFactory.GetGitlabClientAsync();

        var group = await client.Groups.GetAsync(setting.GroupId);
        var members = await client.Groups.GetAllMembersAsync(group.Id);

        _logger.LogInformation("Add projects to group: {GroupName}", group.FullName);
        var readme = setting.ReadmeFile is not null
            ? await File.ReadAllTextAsync(setting.ReadmeFile)
            : null;

        await foreach (var userInfo in _parser.Parse(setting.Filename))
        {
            var projectName = GetProjectName(group, userInfo);
            if (projectName is null)
                continue;
            _logger.LogInformation("Project: {ProjectName}", projectName);

            var user = await GetUserAsync(client, userInfo);
            if (user is null)
            {
                _logger.LogWarning("Not found login for user: {@User}", userInfo);
                continue;
            }

            if (members.FirstOrDefault(x => x.Username == user.Username) is not null)
            {
                _logger.LogWarning("User {@User} already in group", userInfo);
                continue;
            }

            if (group.Projects.FirstOrDefault(x => x.Name == projectName) is not null)
                _logger.LogWarning("Project {ProjectName} already exists", projectName);
            else
            {
                _logger.LogInformation("Create project: {ProjectName}", projectName);
                if (!setting.DryRun)
                    await CreateRepository(client, group, projectName, user, readme);
            }
        }
    }

    private async Task CreateRepository(IGitLabClient client, Group group, string projectName, User user,
        string? readmeFileContent)
    {
        try
        {
            var request = CreateProjectRequest.FromName(projectName);
            request.NamespaceId = group.Id;

            var project = await client.Projects.CreateAsync(request);

            _logger.LogInformation("Add {Login} to project", user.Username);
            await client.Projects.AddMemberAsync(project.Id,
                new AddProjectMemberRequest(AccessLevel.Developer, user.Id));

            if (readmeFileContent is not null)
            {
                _logger.LogInformation("Init readme file for {ProjectName}", projectName);
                await client.Files.AddTextFileAsync(project.Id, "README.md", new CreateFileRequest
                {
                    Content = readmeFileContent,
                    CommitMessage = "Add README.md"
                });
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Failed create project {ProjectName}", projectName);
        }
    }

    private string? GetProjectName(Group group, UserInfo userInfo)
    {
        try
        {
            var formatArgs = new { Group = group, User = userInfo };
            var name = Smart.Format(_settings.Value.RepositoryPattern, formatArgs);

            return _transliterator.Transliterate(name);
        }
        catch (Exception ex)
        {
            _logger.LogError("Failed formatting project name for {@User}. {ExMessage}", userInfo, ex.Message);
            return null;
        }
    }

    private async Task<User?> GetUserAsync(IGitLabClient client, UserInfo userInfo)
    {
        try
        {
            if (userInfo.Login is not null)
                return await client.Users.GetAsync(userInfo.Login);

            var login = _transliterator.Transliterate(userInfo.PartialLogin ?? throw new InvalidDataException());
            var users = await client.Users.GetByFilterAsync(login.ToLower());
            if (users.Count != 1)
                throw new InvalidDataException();
            return users.First();
        }
        catch (Exception ex)
        {
            _logger.LogDebug(ex, "Failed get user: {@User}", userInfo);
            return null;
        }
    }
}