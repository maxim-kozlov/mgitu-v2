using Microsoft.Extensions.Options;
using TranslitSharp;

namespace Mgitu.Console.App.Transliterate;

public class Transliterator : ITransliterator
{
    private readonly TranslitSharp.Transliterator _transliterator;

    public Transliterator(IOptions<TransliterateSettings> settings)
    {
        _transliterator = new TranslitSharp.Transliterator(x =>
        {
            x.ConfigureCyrillicToLatin()
                .OnDuplicateToken(DuplicateTokenBehaviour.TakeLast);
            if (settings.Value.Map is null)
                return;
            foreach (var pair in settings.Value.Map)
                x.AddCharacterMap(pair.Key, pair.Value);
        });
    }

    public string Transliterate(string text)
        => _transliterator.Transliterate(text);
}