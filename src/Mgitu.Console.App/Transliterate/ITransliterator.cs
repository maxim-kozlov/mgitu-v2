namespace Mgitu.Console.App.Transliterate;

public interface ITransliterator
{
    string Transliterate(string text);
}