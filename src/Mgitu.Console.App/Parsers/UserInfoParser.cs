using Mgitu.Console.App.Models;
using Mgitu.Console.App.Parsers.Csv;
using Mgitu.Console.App.Parsers.Excel;
using Microsoft.Extensions.DependencyInjection;

namespace Mgitu.Console.App.Parsers;

public class UserInfoParser : IUserInfoParser
{
    private readonly IServiceProvider _serviceProvider;

    private static readonly Dictionary<string, Type> _parsers = new()
    {
        { ".csv", typeof(UserInfoCsvParser) },

        { ".xls", typeof(UserInfoExcelParser) },
        { ".xlsx", typeof(UserInfoExcelParser) },
        { ".xlsb", typeof(UserInfoExcelParser) },
    };

    public UserInfoParser(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public IAsyncEnumerable<UserInfo> Parse(string filename)
    {
        var extension = Path.GetExtension(filename);
        if (!_parsers.TryGetValue(extension, out var parserType))
            throw new NotSupportedException($"File extension {extension} not support!");

        using var scope = _serviceProvider.CreateScope();
        var parser = (IUserInfoParser)scope.ServiceProvider.GetRequiredService(parserType);
        return parser.Parse(filename);
    }
}