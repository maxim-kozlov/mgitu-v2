namespace Mgitu.Console.App.Parsers.Csv;

public class CsvParserSettings
{
    public string Separator { get; init; } = ";";
}