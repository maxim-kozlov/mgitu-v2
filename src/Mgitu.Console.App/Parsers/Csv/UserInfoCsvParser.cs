using Mgitu.Console.App.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Mgitu.Console.App.Parsers.Csv;

public class UserInfoCsvParser : IUserInfoParser
{
    private readonly ILogger<UserInfoCsvParser> _logger;
    private readonly IOptions<CsvParserSettings> _settings;

    public UserInfoCsvParser(IOptions<CsvParserSettings> settings, ILogger<UserInfoCsvParser> logger)
    {
        _settings = settings;
        _logger = logger;
    }

    public async IAsyncEnumerable<UserInfo> Parse(string filename)
    {
        var settings = _settings.Value;
        _logger.LogDebug("Parse file: {Filename}", filename);
        using var reader = File.OpenText(filename);
        while (!reader.EndOfStream)
        {
            var line = await reader.ReadLineAsync();
            if (string.IsNullOrWhiteSpace(line))
                continue;

            var result = line.Trim().Split(settings.Separator);
            if (result.Length < 2)
            {
                _logger.LogWarning("Invalid line: {Line}", line);
                continue;
            }

            var surname = result[0].Trim();
            var name = result.Length > 2 ? result[1].Trim() : null;
            var patronymic = result.Length > 3 ? result[2].Trim() : null;
            var login = result[^1].Trim().Replace("@", "");

            yield return new UserInfo()
            {
                Name = name,
                Surname = surname,
                Patronymic = patronymic,
                Login = login
            };
        }
    }
}