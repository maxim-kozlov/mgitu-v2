using Mgitu.Console.App.Models;

namespace Mgitu.Console.App.Parsers;

public interface IUserInfoParser
{
    IAsyncEnumerable<UserInfo> Parse(string filename);
}