namespace Mgitu.Console.App.Parsers.Excel;

public class ExcelParserSettings
{
    public bool SkipFirstLine { get; init; } = true;
}