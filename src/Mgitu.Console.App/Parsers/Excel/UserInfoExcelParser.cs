using System.Data;
using ExcelDataReader;
using Mgitu.Console.App.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Mgitu.Console.App.Parsers.Excel;

public class UserInfoExcelParser : IUserInfoParser
{
    private readonly ILogger<UserInfoExcelParser> _logger;
    private readonly IOptions<ExcelParserSettings> _settings;

    public UserInfoExcelParser(ILogger<UserInfoExcelParser> logger,
        IOptions<ExcelParserSettings> settings)
    {
        _logger = logger;
        _settings = settings;
    }

    public async IAsyncEnumerable<UserInfo> Parse(string filename)
    {
        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

        var settings = _settings.Value;
        _logger.LogDebug("Parse file: {Filename}", filename);

        await using var stream = File.Open(filename, FileMode.Open, FileAccess.Read);
        using var reader = ExcelReaderFactory.CreateReader(stream);

        var result = reader.AsDataSet(new ExcelDataSetConfiguration()
        {
            UseColumnDataType = true,
            ConfigureDataTable = tableReader => new ExcelDataTableConfiguration
            {
                UseHeaderRow = true
            }
        });

        foreach (var table in result.Tables.Cast<DataTable>())
        {
            foreach (var row in table.Rows.Cast<DataRow>())
            {
                UserInfo? userInfo = null;
                try
                {
                    var number = row.ItemArray[0];
                    var surname = row.Field<string>(1);
                    var name = row.Field<string>(2);
                    var patronymic = row.Field<string?>(3);
                    var login = row.Field<string>(4) ?? throw new InvalidDataException();

                    userInfo = new UserInfo
                    {
                        Surname = surname,
                        Name = name,
                        Patronymic = patronymic,
                        PartialLogin = login
                    };
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Failed parse line");
                    continue;
                }

                yield return userInfo;
            }
        }
    }
}