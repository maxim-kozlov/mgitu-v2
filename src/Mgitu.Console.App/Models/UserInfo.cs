namespace Mgitu.Console.App.Models;

public record UserInfo
{
    public string? Name { get; init; }

    public string? Surname { get; init; }

    public string? Patronymic { get; init; }

    public string? Login { get; init; }

    public string? PartialLogin { get; init; }
}