using System.ComponentModel.DataAnnotations;
using CommandLine;
using CommandLine.Text;
using Mgitu.Console.App.Gitlab;
using Microsoft.Extensions.Options;

namespace Mgitu.Console.App;

public class CliSettings
{
    /// <summary>
    /// Общие настройки гитлаба
    /// </summary>
    public GitlabSettings GitlabSettings { get; init; }

    /// <summary>
    /// Группа гитлаба в которую добавляются репозитории
    /// </summary>
    /// <remarks>
    /// Если int - id группы
    /// Иначе - путь группы
    /// </remarks>
    [Required]
    [Option("group", Required = true, HelpText = "Gitlab group (id or path)")]
    public string GroupId { get; init; }

    /// <summary>
    /// Файл содержащий список логинов
    /// </summary>
    [Required]
    [Option('f', "file", Required = true,
        HelpText = "Path to users")]
    public string Filename { get; init; }

    /// <summary>
    /// Шаблон формирования репозитория
    /// </summary>
    [Required]
    [Option("pattern", Required = false, HelpText = "Pattern creating repository")]
    public string? RepositoryPattern { get; init; }

    /// <summary>
    /// Readme файл в создаваемом репозитории
    /// </summary>
    [Option("readme", Required = false, HelpText = "Path to readme file")]
    public string? ReadmeFile { get; init; }

    /// <summary>
    /// Dry run
    /// </summary>
    [Option("dry", Default = false, Required = false, HelpText = "Dry run")]
    public bool DryRun { get; init; }

    [Usage]
    public static IEnumerable<Example> Examples =>
        new List<Example>
        {
            new("Create repositories to users",
                new CliSettings
                {
                    GroupId = "1234",
                    Filename = "students.xlsx",
                    GitlabSettings = new GitlabSettings
                    {
                        Login = "<gitlab-login>",
                        Token = "<gitlab-token>"
                    }
                })
        };
}