using System.ComponentModel.DataAnnotations;
using CommandLine;

#pragma warning disable CS8618

namespace Mgitu.Console.App.Gitlab;

public record GitlabSettings
{
    [Required]
    [Option("host", Required = false, HelpText = "Gitlab host")]
    public Uri Host { get; init; }

    [Option("token", Required = false, HelpText = "Gitlab token")]
    public string? Token { get; init; }

    [Option('u', "user", Required = false, HelpText = "Gitlab login")]
    public string? Login { get; init; }

    [Option('p', "password", Required = false, HelpText = "Gitlab password")]
    public string? Password { get; init; }
}