using GitLabApiClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Mgitu.Console.App.Gitlab;

public class GitLabClientFactory
{
    private readonly IOptions<GitlabSettings> _settings;
    private readonly ILogger<GitLabClientFactory> _logger;

    public GitLabClientFactory(IOptions<GitlabSettings> settings, ILogger<GitLabClientFactory> logger)
    {
        _settings = settings;
        _logger = logger;
    }

    public async ValueTask<IGitLabClient> GetGitlabClientAsync()
    {
        var settings = _settings.Value;
        if (settings.Token is not null)
            return new GitLabClient(settings.Host.ToString(), settings.Token);

        var client = new GitLabClient(settings.Host.ToString());
        var token = await client.LoginAsync(settings.Login, settings.Password);
        _logger.LogWarning("Please, use access token: {Token}", token.AccessToken);
        return client;
    }
}