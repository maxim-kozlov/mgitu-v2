using System.Reflection;
using System.Text;
using CommandLine;
using CommandLine.Text;
using Microsoft.Extensions.Configuration;

namespace Mgitu.Console.App.Extensions;

public static class CommandLineExtensions
{
    public static IConfigurationBuilder AddCliArgs<TOption>(this IConfigurationBuilder builder, string[] args)
    {
        var switchMappings = GetSwitchMappings<TOption>();
        return builder.AddCommandLine(args, switchMappings);
    }

    public static HelpText CustomBuild<TOption>(this HelpText helpText)
    {
        var helpInfo = new List<(string Name, string Help)>();
        foreach (var property in typeof(TOption).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            AddOptionHelpCli(helpText, property, helpInfo);

        var maxLenght = helpInfo.Max(x => x.Name.Length);
        foreach (var (name, help) in helpInfo)
        {
            var line = new StringBuilder("  ")
                .Append(name.PadRight(maxLenght + 4))
                .Append(help).ToString();
            helpText.AddPreOptionsLine(line);
        }
        return helpText;
    }

    private static Dictionary<string, string> GetSwitchMappings<TOption>()
    {
        var switchMappings = new Dictionary<string, string>();
        foreach (var property in typeof(TOption).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            SetSwitchMappings(switchMappings, property);
        return switchMappings;
    }

    private static void SetSwitchMappings(IDictionary<string, string> switchMappings,
        PropertyInfo propertyInfo,
        string? rootPath = null)
    {
        var path = GetFullPath(rootPath, propertyInfo.Name);
        var opt = propertyInfo.GetCustomAttribute<OptionAttribute>();
        if (opt is not null)
        {
            if (!string.IsNullOrWhiteSpace(opt.ShortName))
                switchMappings[$"-{opt.ShortName}"] = path;
            if (!string.IsNullOrWhiteSpace(opt.LongName))
                switchMappings[$"--{opt.LongName}"] = path;
        }
        else
        {
            var properties = propertyInfo.PropertyType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
                SetSwitchMappings(switchMappings, property, GetFullPath(rootPath, propertyInfo.Name));
        }
    }

    private static string GetFullPath(string? path, string propertyName)
    {
        return path is null
            ? propertyName
            : $"{path}:{propertyName}";
    }
    
    
    private static void AddOptionHelpCli(HelpText helpText, 
        PropertyInfo propertyInfo, 
        IList<(string Name, string Help)> options)
    {
        var opt = propertyInfo.GetCustomAttribute<OptionAttribute>();
        if (opt is not null)
        {
            // optionsHelp.Append("  ");
            var optionName = GetOptionName(opt, helpText.AddDashesToOption);

            var help = new StringBuilder()
                .AppendFormatWhen(opt.Required, "{0} ", helpText.SentenceBuilder.RequiredWord())
                .Append(opt.HelpText)
                .ToString();
            options.Add((optionName, help));
        }
        else
        {
            var properties = propertyInfo.PropertyType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
                AddOptionHelpCli(helpText, property, options);
        }
    }
    
    private static string GetOptionName(OptionAttribute specification, bool addDashesToOption)
    {
        var optionsHelp = new StringBuilder();
        if (specification.ShortName.Length > 0)
        {
            optionsHelp.AppendWhen(addDashesToOption, "-")
                .Append(specification.ShortName)
                .AppendFormatIfNeed("{0}", specification.MetaValue)
                .AppendWhen(specification.LongName.Length > 0, ", ");
        }
        if (specification.LongName.Length > 0)
        {
            optionsHelp.AppendWhen(addDashesToOption, "--")
                .Append(specification.LongName)
                .AppendFormatIfNeed("={0}", specification.MetaValue);
        }
        return optionsHelp.ToString();
    }
    
    private static StringBuilder AppendWhen(this StringBuilder builder, bool condition, string value)
    {
        if (condition)
            builder.Append(value);
        return builder;
    }
    
    private static StringBuilder AppendFormatWhen(this StringBuilder builder, bool need, string format, string value)
    {
        if (need)
            builder.AppendFormat(format, value);
        return builder;
    }
    
    private static StringBuilder AppendFormatIfNeed(this StringBuilder builder, string format, string value)
    {
        if (value.Length > 0)
            builder.AppendFormat(format, value);
        return builder;
    }
}