using GitLabApiClient.Models.Groups.Responses;
using Mgitu.Console.App;
using Mgitu.Console.App.Models;
using SmartFormat;

namespace Mgitu.Tests;

public class FormatTests
{
    [Fact]
    public void RepositoryPatternTest()
    {
        var args = new
        {
            Group = new Group
            {
                Name = "GroupName"
            },
            User = new UserInfo
            {
                Login = "Login",
                Surname = "Surname",
                Name = "Name"
            }
        };

        var pattern =
            "{Group.Name.ToLower}-{User.Surname?.ToLower}-{User.Name?.ToLower}{User.Patronymic?.ToLower:choose(null):|-{}}";
        Assert.Equal("groupname-surname-name", Smart.Format(pattern, args));
    }

    [Fact]
    public void RepositoryPatternWithPatronymicTest()
    {
        var args = new
        {
            Group = new Group
            {
                Name = "GroupName"
            },
            User = new UserInfo
            {
                Login = "Login",
                Surname = "Surname",
                Name = "Name",
                Patronymic = "Patronymic"
            }
        };

        var pattern =
            "{Group.Name.ToLower}-{User.Surname?.ToLower}-{User.Name?.ToLower}{User.Patronymic?.ToLower:choose(null):|-{}}";
        Assert.Equal("groupname-surname-name-patronymic", Smart.Format(pattern, args));
    }
}